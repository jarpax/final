﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{

    public CharacterController controller;
    public float speed = 10f;
    public float gravity = -9.8f;
    public float jump = 3;
    public Transform groundCheck;
    public float groundDistnace = 0.3f;
    public LayerMask groundMask;
    Vector3 velocity;
    bool isGrounded;

    public VariableJoystick variableJoystick;
    //public Rigidbody rb;


    void Start()
    {
        
    }

    void Update()
    {

#if UNITY_STANDALONE
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistnace, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        /*
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jump * -2 * gravity);
        }
        */

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity*Time.deltaTime);
#endif

#if UNITY_ANDROID

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistnace, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        Vector3 move = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        controller.Move(move * speed * Time.deltaTime);

        /*
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jump * -2 * gravity);
        }
        */

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);




        /*isGrounded = Physics.CheckSphere(groundCheck.position, groundDistnace, groundMask);

        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        rb.AddForce(direction * speed * Time.fixedDeltaTime, ForceMode.VelocityChange);
        */

#endif

    }
}
