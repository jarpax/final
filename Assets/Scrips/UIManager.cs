﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class UIManager : MonoBehaviour
{

    public AudioSource clip;
    public GameObject panelPrincipal;

    public void PanelPrincipal()
    {

    }

    public void Iniciar()
    {

        SceneManager.LoadScene("Level1");

    }

    public void SelectLevel()
    {
        SceneManager.LoadScene("SelectLevel");

    }

    public void End()
    {
        SceneManager.LoadScene("Menu");

    }

    public void Sound()
    {
        SceneManager.LoadScene("Sonido");

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PlaySoundButton()
    {
        clip.Play();
    }

}
