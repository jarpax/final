﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Kontatras : MonoBehaviour
{
    float cuenta = 0f;
    float startime =120f;
    float endtime = 0f;

    [SerializeField] Text cuantaAtras;

    private void Start()
    {
        cuenta = startime;
    }

    private void Update()
    {
        cuenta -= 1 * Time.deltaTime;
        endtime += 1 * Time.deltaTime;
        cuantaAtras.text = cuenta.ToString("0");
        PlayerPrefs.SetFloat("Puntaje", endtime);

        if (cuenta <= 60)
        {
            cuantaAtras.color = Color.yellow;
        }

        if (cuenta <= 30)
        {
            cuantaAtras.color = Color.red;
        }

        if (cuenta <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }

}
