﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class SeguirEnemigo : MonoBehaviour
{

    public NavMeshAgent navMeshAgent;

    public GameObject seguir;


    void Start()
    {

    }

    
    void Update()
    {
        navMeshAgent.destination = seguir.transform.position;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("GameOver");

        }
    }


}
